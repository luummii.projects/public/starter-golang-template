package database

import (
	"database/sql"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/hako/branca"

	// This is postgres drivers
	_ "github.com/jackc/pgx/stdlib"
)

// DataBase contains the core business logic separated from the transport layer.
type DataBase struct {
	DB    *sql.DB
	codec *branca.Branca
}

// Init - new controller implementation.
func Init() *DataBase {
	db, err := sql.Open("pgx", os.Getenv("DATABASE_URL"))

	if err != nil {
		panic("ERROR CONNECT TO DATABASE! CHECK PARAMS FOR CONNECT (url, password and more...)")
	}

	err = db.Ping()
	if err != nil {
		log.Panicf("ERROR DB PING:%s\n", err)
	}

	codec := branca.NewBranca(os.Getenv("BRANCA"))
	codec.SetTTL(uint32(TokenLifespan.Seconds()))

	log.Println("CONNECT DATABASE IS SUCCESS")

	file, err := ioutil.ReadFile("database/migrations/schema.sql")
	if err != nil {
		log.Fatalln(err)
	}
	requests := strings.Split(string(file), ";")

	for _, request := range requests {
		if len(request) > 0 {
			if _, err := db.Exec(request); err != nil {
				log.Panicf("ERROR CREATE DB:%s\n", err)
			}
		}
	}

	return &DataBase{
		DB:    db,
		codec: codec,
	}
}
